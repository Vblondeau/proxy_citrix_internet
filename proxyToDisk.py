#****************************************************
#                                                   *
#               HTTP PROXY                          *
#               Version: 1.0                        *
#               Author: Luu Gia Thuy                *
#                                                   *
#****************************************************

import os,sys,thread,socket,logging
from datetime import datetime
import os, errno, time
import select
import shutil
import fcntl

 
logging.basicConfig(level=logging.INFO)

#********* CONSTANT VARIABLES *********
BACKLOG = 50            # how many pending connections queue will hold
BLOCK_READ_SIZE = 256 * 1024  # max number of bytes we receive at once
BLOCKED = []            # just an example. Remove with [""] for no blocking at all.

#**************************************
#********* MAIN PROGRAM ***************
#**************************************
def main():

    # check the length of command running
    if (len(sys.argv)<2):
        logging.info ("No port given, using :8080 (http-alt)") 
        port = 8080
    else:
        port = int(sys.argv[1]) # port from argument

    # host and port info.
    host = ''               # blank for localhost
    
    logging.info ( "Proxy Server Running on %s:%d" %(host,port ) )

    try:
        # create a socket
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        s. setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        # associate the socket to host and port
        s.bind((host, port))

        # listenning
        s.listen(BACKLOG)
    
    except socket.error as   message :
        if s:
            s.close()
        logging.error ( "Could not open socket: %s", (message ))
        sys.exit(1)

    # get the connection from client
    while 1:
        conn, client_addr = s.accept()

        
        # create a thread to handle request
        thread.start_new_thread(proxy_thread, (conn, client_addr))
        
    s.close()
#************** END MAIN PROGRAM ***************

def ensureCreated(directory):
    try:
      os.makedirs(directory)
    except OSError as e:
      if e.errno != errno.EEXIST:
        raise

def cleanup(directory):
  time.sleep( 5 )
  logging.info("cleaning up %s" % (directory))
  shutil.rmtree(directory)
def setfilenonblocking(f):
  fd = f.fileno()
  flag = fcntl.fcntl(fd, fcntl.F_GETFL)
  fcntl.fcntl(fd, fcntl.F_SETFL, flag | os.O_NONBLOCK)
#*******************************************
#********* PROXY_THREAD FUNC ***************
# A thread to handle request from browser
#*******************************************
def proxy_thread(conn, client_addr):

    baseDir = os.environ['HOME']+"/proxyrequests"

    requestTime= datetime.now().strftime("%Y%m%dT%H%M%S%f" )
    filesDir = baseDir + "/" + requestTime
    
    ensureCreated(filesDir)
    logging.info("initiated conversation in "+filesDir)

    
    exchangeCount=1
    requestFile = filesDir+ "/request.txt"
    responseFile = filesDir+ "/response.txt"
    closedFile = filesDir+ "/closed.txt"
    
    output = open(requestFile, 'wb') 
    input = None
    errormessage = None
    conn.setblocking(0)
    read_descriptors = [ conn]
    no_file_read_descriptors = [ conn]
    write_descriptors = [ conn ]
    sendBuffer = bytearray(0)
    writeBuffer = bytearray(0)
    isClosed = False
    try:
      while True:
       write_descriptors_to_pass = []
       if len(sendBuffer) >0 :
          write_descriptors_to_pass = write_descriptors
       read_descriptors_to_pass = no_file_read_descriptors
       timeout = 0.1
       readable, writable, exceptional  = select.select(read_descriptors_to_pass, write_descriptors_to_pass , [] , timeout)
       #logging.debug("(%d , %d, %d ) = select.select (%d, %d, %d)"% ( len(readable), len(writable), len(exceptional), len(read_descriptors_to_pass), len(write_descriptors_to_pass), 0))
       if  input is None :
          if  os.path.isfile(responseFile) :
            input  = open(responseFile, 'rb')
            setfilenonblocking(input)
            read_descriptors.append(input)
       if conn in readable :
         try:
            #logging.debug("about to read socket")
            requestBytes = conn.recv(BLOCK_READ_SIZE)
            if requestBytes:
              output.write(requestBytes)
              output.flush()
              logging.debug("wrote %d bytes to file %s" %( len (requestBytes), requestFile))
            else:
              logging.debug ( "detected closed socket on read")
              isClosed = True
         except socket.error, e:
             if e[0] == 104 :
                logging.debug ( "detected closed socket on read (by exception)")
                isClosed = True
       if  input is not None :
              #logging.debug("about to read file")
              responseBytes = input.read(BLOCK_READ_SIZE)
              if len(responseBytes) >0:
                logging.debug ( "got  %d bytes from file %s  "% (len(responseBytes),responseFile))
                sendBuffer.extend(responseBytes)
       if conn in writable and  len(sendBuffer) >0:
              sent = conn.send(sendBuffer)
              sendBuffer = sendBuffer[sent:]
              logging.debug ( "wrote %d bytes to socket remaining %d "% (sent, len(sendBuffer)))

       if isClosed and  len(sendBuffer) ==0:
         break
    except:
        errormessage =  sys.exc_info()[0]
        logging.error("caught %s  "% (str(errormessage) ), exc_info=True)
    finally:
      conn.close()
      output.close()

      if input is not None:
        input.close()
      if not os.path.isfile(closedFile):
        with open(closedFile, 'wb') as closedFh:
          logging.debug("created %s" % (closedFile))
      logging.debug ( "closed conversation "+ filesDir )
      cleanup( filesDir )
      
#********** END PROXY_THREAD ***********
    
if __name__ == '__main__':
    main()





