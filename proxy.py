#****************************************************
#                                                   *
#               HTTP PROXY                          *
#               Version: 1.0                        *
#               Author: Luu Gia Thuy                *
#                                                   *
#****************************************************

import os,sys,thread,socket,logging
from datetime import datetime
import os, errno, time

 
logging.basicConfig(level=logging.DEBUG)

#********* CONSTANT VARIABLES *********
BACKLOG = 50            # how many pending connections queue will hold
MAX_DATA_RECV = 999999  # max number of bytes we receive at once
DEBUG = True            # set to True to see the debug msgs
BLOCKED = []            # just an example. Remove with [""] for no blocking at all.

#**************************************
#********* MAIN PROGRAM ***************
#**************************************
def main():

    # check the length of command running
    if (len(sys.argv)<2):
        print ("No port given, using :8080 (http-alt)") 
        port = 8080
    else:
        port = int(sys.argv[1]) # port from argument

    # host and port info.
    host = ''               # blank for localhost
    
    print ( "Proxy Server Running on %s:%d" %(host,port ) )

    try:
        # create a socket
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # associate the socket to host and port
        s.bind((host, port))

        # listenning
        s.listen(BACKLOG)
    
    except socket.error as   message :
        if s:
            s.close()
        print ( "Could not open socket: %s", (message ))
        sys.exit(1)

    # get the connection from client
    while 1:
        conn, client_addr = s.accept()

        
        # create a thread to handle request
        thread.start_new_thread(proxy_thread, (conn, client_addr))
        
    s.close()
#************** END MAIN PROGRAM ***************

def ensureCreated(directory):
    try:
      os.makedirs(directory)
    except OSError as e:
      if e.errno != errno.EEXIST:
        raise


#*******************************************
#********* PROXY_THREAD FUNC ***************
# A thread to handle request from browser
#*******************************************
def proxy_thread(conn, client_addr):

    baseDir = "/cygdrive/s/socks"

    requestTime= datetime.now().strftime("%Y%m%dT%H%M%S%f" )
    requestDir = baseDir + "/" + requestTime+ "/request"
    responseDir = baseDir + "/" + requestTime+ "/response"
    
    #ensureCreated(requestDir)
    #ensureCreated(responseDir)

  


    # get the request from browser
    requestBytes = conn.recv(MAX_DATA_RECV)
    requestFile = requestDir+ "/request.txt"
    #with open(requestFile, 'wb') as output:
    #  output.write(requestBytes)
    
    request = requestBytes.decode('utf-8')

    # parse the first line
    first_line = request.split('\n')[0]
    other_lines = request.split('\n', 1)[1]

    # get url
    url = first_line.split(' ')[1]
    logging.debug("url is %s" % (url))

     
    # print "URL:",url
    # print
    
    
    responseFile = responseDir+ "/response.txt"
    proxyserver = "127.0.0.1"
    proxyport = 5865
    
    try:
        # create a socket to connect to the web server
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  
        logging.debug("Connecting to %s %s " % (proxyserver, proxyport))
        s.connect((proxyserver, proxyport))
        logging.debug("Connected to %s %s " % (proxyserver, proxyport))
        #s.send(requestBytes)         # send request to proxyserver
        s.send(first_line+"\n")         # send request to proxyserver
        s.send("Connection: close\r\n")         # send request to proxyserver
        s.send(other_lines+"\n")         # send request to proxyserver
        logging.debug("proxied request to %s %s " % (proxyserver, proxyport))
        #with open(responseFile, 'wb') as output2:
        while 1:
            # receive data from web server
            data = s.recv(MAX_DATA_RECV)

            if (len(data) > 0):
                # send to browser
                #output2.write(data)
                conn.send(data)
            else:
                break
        s.close()
        conn.close()
        logging.debug("sockets closed"  )
    except socket.error as message :
        if s:
            s.close()
        if conn:
            conn.close()
        logging.error("caught %s  "% (str(message) ))
        logging.debug("Peer Reset %s  "% (first_line ))
        sys.exit(1)
#********** END PROXY_THREAD ***********
    
if __name__ == '__main__':
    main()




