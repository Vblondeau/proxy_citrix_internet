#!/bin/bash

set -e


/usr/bin/find  src/main/java -name  "*.java" > sources.txt


[[ ! -f scp-sync.ori.jar ]] && cp target/scp-sync.jar scp-sync.ori.jar

javac  -source 1.7  -target 1.7 -d target/classes -classpath scp-sync.ori.jar @sources.txt

mkdir tmp
cd tmp
jar xf ../scp-sync.ori.jar
cp -r ../target/classes/* .
jar cfM ../target/scp-sync.jar META-INF/MANIFEST.MF com
cd ..
rm -rf tmp
cp -f target/scp-sync.jar '//silnas100/citrix$/homedir_cesario/'"$USERNAME".PRODGRP/scp-sync.jar