package com.cagip.bigdata.scprsync;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.net.URL;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Logs {
    public static Logger logger = Logger.getLogger(Logs.class.getName());

    static LogFormatter formatter = new LogFormatter();

    public static void globallySetupLogs() {
        // log4j logging
        Throwable configurationIssue = null;
        URL configUrl = Thread.currentThread().getContextClassLoader().getResource("logging.properties");
        if (configUrl != null) {
            try {
                LogManager.getLogManager().reset();
                InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("logging.properties");
                try {
                    LogManager.getLogManager().readConfiguration(is);
                } finally {
                    is.close();
                }
                logger = Logger.getLogger(Logs.class.getName());
                logger.info("logging.properties found at " + configUrl.toString());
            } catch (Exception e) {
                configurationIssue = e;
            }
        } else {
            configurationIssue = new IOException("logging.properties not found in classpath");
        }

    }

    public static void justSetLevels() {
        // log4j logging

        ConsoleHandler myhandler = new ConsoleHandler();
        myhandler.setLevel(Level.FINEST);

        myhandler.setFormatter(formatter);
        Logger jvmmonloggerbase = Logger.getLogger("com.cagip");
        jvmmonloggerbase.addHandler(myhandler);
        jvmmonloggerbase.setUseParentHandlers(false);

        Throwable configurationIssue = null;
        URL configUrl = Thread.currentThread().getContextClassLoader().getResource("logging.properties");
        if (configUrl != null) {
            try {
                InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("logging.properties");
                try {
                    LineNumberReader rdr = new LineNumberReader(new InputStreamReader(is));
                    String line;
                    Pattern commentPat = Pattern.compile("^#.*$");
                    Pattern levelPat = Pattern.compile("^\\s*(.*).level\\s*=\\s*([^\\s]*)\\s*$");
                    while ((line = rdr.readLine()) != null) {
                        if (commentPat.matcher(line).matches()) {
                            continue;
                        }
                        Matcher levelMatcher = levelPat.matcher(line);
                        if (levelMatcher.matches()) {
                            String logger = levelMatcher.group(1);
                            String level = levelMatcher.group(2);
                            //dont touch root logger!
                            if (logger.equals(""))
                                continue;
                            //dont touch ConsoleHandler!
                            if (logger.equals("java.util.logging.ConsoleHandler"))
                                continue;
                            setToLevel(logger, level);


                        }
                    }
                } finally {
                    is.close();
                }
                logger = Logger.getLogger(Logs.class.getName());
                logger.info("logging.properties found at " + configUrl.toString());
            } catch (Exception e) {
                configurationIssue = e;
            }
        } else {
            configurationIssue = new IOException("logging.properties not found in classpath");
        }

    }

    static Logger getLogger(String name) {
        Logger targetLogger = Logger.getLogger("");
        if (name != null && !name.isEmpty())
            targetLogger = Logger.getLogger(name);
        return targetLogger;
    }

    public static void setToLevel(String name, String level) {
        Level targetLogLev = Level.parse(level);
        setToLevel(name, targetLogLev);

    }

    public static void setToLevel(String name, Level lev) {
        Logger targetLogger = getLogger(name);
        targetLogger.setLevel(lev);

    }

    public static void ensureLevel(String name, Level lev) {
        Logger targetLogger = getLogger(name);
        int targetLogLev = lev.intValue();
        if (targetLogger.getLevel() != null && targetLogger.getLevel().intValue() < targetLogLev) {
            logger.warning("downgrading to level " + lev + " instead of " + targetLogger.getLevel() + " for logger " + name);
        }
        targetLogger.setLevel(lev);
        while (targetLogger != null) {
            Handler[] handlers = targetLogger.getHandlers();
            if (handlers != null) {
                for (Handler h : handlers) {
                    h.setFormatter(formatter);
                    if (h.getLevel().intValue() > targetLogLev) {
                        logger.warning("restauring level " + lev + " for handler " + h.getClass().getName());
                        h.setLevel(lev);
                    }

                }
            }
            targetLogger = targetLogger.getParent();
        }
    }

    public static void ensureWarning(String name) {
        ensureLevel(name, Level.WARNING);
    }

    public static void setDebug(String name) {
        setToLevel(name, Level.FINER);
    }

    public static void setDebug() {
        setDebug("");
    }


    /**
     * method responsible to add the email appender to a specific logger.
     *
     * @param name
     * @param alerter
     */


}
