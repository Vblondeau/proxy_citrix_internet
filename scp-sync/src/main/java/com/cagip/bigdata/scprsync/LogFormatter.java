package com.cagip.bigdata.scprsync;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public final class LogFormatter extends Formatter {

    private static final String LINE_SEPARATOR = System.getProperty("line.separator");

    DateFormat fmt = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.LONG);

    @Override
    public String format(LogRecord record) {
        StringBuilder sb = new StringBuilder();
//<10 avr. 2015 09 h 53 CEST> <Notice> <Server> <BEA-002613> <Channel "Default[11]" is now listening on fe80:0:0:0:dc57:2bae:a1e:6...>
        sb.append("<");
        sb.append(fmt.format(new Date(record.getMillis())));
        sb.append("> <");
        sb.append(record.getLevel().toString());
        sb.append("> <");
        sb.append(record.getLoggerName().replaceAll(".*\\.", ""));
        sb.append("> <");
        sb.append(record.getMessage());
        sb.append(">");
        sb.append(LINE_SEPARATOR);

        if (record.getThrown() != null) {
            try {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                record.getThrown().printStackTrace(pw);
                pw.close();
                sb.append(sw.toString());
            } catch (Exception ex) {
                // ignore
            }
            sb.append(LINE_SEPARATOR);
        }

        return sb.toString();
    }
}
