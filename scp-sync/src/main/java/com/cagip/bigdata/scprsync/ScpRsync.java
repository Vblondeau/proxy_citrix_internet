package com.cagip.bigdata.scprsync;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.UserInfo;

public class ScpRsync {

	public static Logger logger = Logger.getLogger(ScpRsync.class.getName());

	@Parameter(names = { "--remoteFolder" }, description = "remote folder")
	String remoteFolder = ".";
	@Parameter(names = { "--localFolder" }, description = "local folder")
	String localFolder = ".";

	@Parameter(names = { "--remoteHost" }, description = "sftp remote host")
	String host = "central.logintimecontrol.com";

	@Parameter(names = { "-u", "--user" }, description = "sftp username")
	String user = "ltcadmin";
	@Parameter(names = { "-p", "--password" }, description = "sftp password")
	String password = "E8JPPd6ewcnLXOW8ciJj";
	@Parameter(names = { "-P", "--password-file" }, description = "sftp password file")
	String passwordFile = null ;


	String[] args;

	Set<String> ignoreDirs = new HashSet<>();
	Set<String> watchDeletionDirs = new HashSet<>();

	public ScpRsync(String[] args) {
		this.args = args;
	}

	@Parameter(names = { "--verbose", "-v" }, description = "verbose")
	private boolean verbose = false;

	@Parameter(names = { "--no-purge"}, description = "disable purge")
	private boolean noPurge = false;

	private void init() throws Exception {

		JCommander.newBuilder().addObject(this).build().parse(args);

		Logs.justSetLevels();

		if (verbose)
			Logs.setDebug("com.cagip.bigdata.scprsync");

        if ( passwordFile != null) {
            File passwordFileFile = new File(passwordFile);
            byte[] passwordBytes = Files. readAllBytes (passwordFileFile.toPath());
            password = new String(passwordBytes, StandardCharsets.UTF_8);
        }
		logger.fine("remoteFolder " + remoteFolder);
		logger.fine("localFolder " + localFolder);
		logger.fine("remoteHost " + host);
		logger.fine("user " + user);

	}

	public static void main(String[] args) throws Exception {

		ScpRsync me = new ScpRsync(args);

		me.init();
		me.run();

	}

	Session session;

	private void purgeDir(final String dirName)  {
        watchDeletionDirs.remove(dirName);
        if ( noPurge )
            return;
        // then, when you want to schedule a task
        logger.info("Purging dir "+dirName);
        String localFilesDir = localFolder + "/" + dirName;
        File localFilesDirFile = new File(localFilesDir);
        try {
        
            deleteDirectory(localFilesDirFile);
        } catch (Exception e) {
            logger.log(Level.WARNING, "caught exception purging  "+localFilesDir, e);
        }
 
    }
    
    void  deleteDirectory(File f) throws  Exception {
        
      if (f.isDirectory()) {
        File[] children = f.listFiles();
        for (File c : children )
          deleteDirectory(c);
        logger.fine("Purging dir  "+f);
      } else {
        logger.fine("Purging file "+f);
      }
      Files.delete(f.toPath());
      if ( f.exists()) {
          logger.warning("file "+f+" still exists although deleted successfully !!");
      } else {
          logger.fine("file "+f+" is deleted");
      }
    }

 	private void run() throws Exception {
 

// and finally, when your program wants to exit
		JSch jsch = new JSch();

		int port = 22;

		session = jsch.getSession(user, host, port);

		// username and password will be given via UserInfo interface.

		try {
			UserInfo ui = new MyUserInfo();
			session.setUserInfo(ui);

			session.connect();

			Channel channel = session.openChannel("sftp");
			channel.connect();
			try {
				ChannelSftp c = (ChannelSftp) channel;

				c.cd(remoteFolder);
				logger.info(String.format("Watching %s@%s:%s", user, host, remoteFolder));
				Vector<ChannelSftp.LsEntry> v = c.ls(".");
				for (ChannelSftp.LsEntry item : v) {
					String name = item.getFilename();
					if (item.getAttrs().isDir() && !name.endsWith(".processed")) {
						ignoreDirs.add(name);
					}

				}

				while (true) {
					v = c.ls(".");
					Set<String> seenDirs = new HashSet<>();

					for (ChannelSftp.LsEntry item : v) {
						String name = item.getFilename();
						seenDirs.add(name);
						if (item.getAttrs().isDir() && !name.endsWith(".processed")) {
							if (!ignoreDirs.contains(name)) {
								ignoreDirs.add(name);
								watchDeletionDirs.add(name);
								logger.info("new dir found " + name);
								Thread t = new Thread(new RequestProcessor(name));
								t.start();
							}
						}

					}
					Set<String> mustDeleteDirs = new HashSet<>();
					mustDeleteDirs.addAll(watchDeletionDirs);
					mustDeleteDirs.removeAll(seenDirs);
					for (String name : mustDeleteDirs) {
						purgeDir(name);
					}
					Thread.sleep(200);

				}
			} finally {
				channel.disconnect();
			}
		} catch (Exception e) {
					logger.log(Level.SEVERE, "caught exception, exiting  ", e);
                    throw e;
		} finally {
			session.disconnect();
 
		}

	}

	private void ensureCreated(String dir) {
		File me = new File(dir);
		if (me.exists())
			return;
		boolean created = me.mkdirs();
		if (!created)
			throw new RuntimeException("Could not create dir " + dir);
		logger.fine("created " + me);

	}

	public class RequestProcessor implements Runnable {

		String dirName;

		public RequestProcessor(String d) {
			dirName = d;
		}

		@Override
		public void run() {
            boolean copyDoneFile = false;
			try {
				/*
				 * the idea is to copy request files and wait for response files
				 */
				String remoteFilesDir = remoteFolder + "/" + dirName;
				String remoteRequestFile = remoteFilesDir + "/request.txt";
				String remoteResponseFile = remoteFilesDir + "/response.txt";
				String remoteResponseDoneFile = remoteFilesDir + "/closed.txt";
				String remoteResponseBadFile = remoteFilesDir + "/response.bad";

				String localFilesDir = localFolder + "/" + dirName;
				String localRequestFile = localFilesDir + "/request.txt";
				String localResponseFile = localFilesDir + "/response.txt";
				String localResponseDoneFile = localFilesDir + "/closed.txt";
				String localResponseBadFile = localFilesDir + "/response.bad";
				ensureCreated(localFilesDir);

				final File localResponseFileFile = new File(localResponseFile);
				final File localResponseDoneFileFile = new File(localResponseDoneFile);
				final File localResponseBadFileFile = new File(localResponseBadFile);

				Channel channel = session.openChannel("sftp");
				channel.connect();
				ChannelSftp c = null;
				InputStream remoteRequestStream = null;
				OutputStream localRequestStream = null;
				InputStream localResponseStream = null;
				OutputStream remoteResponseStream = null;
 				long requestReadOffset = 0;
				try {
					c = (ChannelSftp) channel;

					while (!remoteFileExists(c, remoteRequestFile)) {
						Thread.sleep(200);
					}
					logger.fine("remote request file found " + remoteRequestFile);

 					remoteRequestStream = c.get(remoteRequestFile);
					localRequestStream = new FileOutputStream(localRequestFile);

					boolean done = false;
					boolean remotelydone = false;
					while (!done) {
						if ( true ) {
							Field closedField = remoteRequestStream.getClass().getDeclaredField("closed");
							closedField.setAccessible(true);
							if ( closedField.getBoolean(remoteRequestStream)) {
								//just  reopen
                                SftpException caught = null;
								try {
                                    logger.finest(String.format("reopening file %s at pos %d",remoteRequestFile ,requestReadOffset ));
                                    remoteRequestStream.close();
                                    remoteRequestStream = c.get(remoteRequestFile, null,  requestReadOffset);
                                } catch (  SftpException e) {
                                    caught = e;
                                    logger.fine("Caught internally "+e);
                                }
                                if ( localResponseDoneFileFile.exists() ) {
                                    if ( ! remotelydone ) {
                                        copyDoneFile = true;
                                    }
                                    done =  true;
                                } else if (!remoteFileExists(c, remoteFilesDir) ||   remoteFileExists(c, remoteResponseDoneFile)) {

                                    done =  true;
                                    remotelydone = true;
                                    //create local done file
                                    FileOutputStream fos = new FileOutputStream(localResponseDoneFileFile);
                                    fos.close();
                                    logger.fine(String.format("created local file %s ",localResponseDoneFile ));

                                }
                                         
                                if (   caught != null ) {
                                    if ( done )
                                        break;
                                    throw caught;                                    
                                }
							}
						}

						int copied = copyStreamNonBlocking(remoteRequestStream, localRequestStream);
						if (copied > 0) {
							logger.fine(String.format("Copied %d bytes to local file %s", copied, localRequestFile));
							requestReadOffset+= copied;
						}
						if (localResponseStream == null) {
							if (localResponseFileFile.exists()) {
								logger.fine("local response file found " + localResponseFileFile);
								localResponseStream = new FileInputStream(localResponseFileFile);
 								remoteResponseStream = c.put(remoteResponseFile);

							}
						}
						if (localResponseStream != null) {
							int copied2 = copyStreamNonBlocking(localResponseStream, remoteResponseStream);
							if (copied2 > 0) {
								copied += copied2;
								logger.fine(String.format("Copied %d bytes to remote file %s", copied2,
										remoteResponseFile));
							}
						}
						if (copied == 0) {

							Thread.sleep(100);
						}
					}

					logger.fine("fully copied request/response to remote  " + dirName);
                    if (copyDoneFile ) {
                        OutputStream osd = c.put(remoteResponseDoneFile);
                        FileInputStream fis = new FileInputStream(localResponseDoneFileFile);
                        copyStream(fis, osd, true);
                        logger.fine("copied marker to remote  " + remoteResponseDoneFile);
                    }
				} catch (Exception e) {
					logger.log(Level.SEVERE, "caught exception, handling error  ");
					logger.log(Level.FINE, " exception details  ", e);
					try {
						OutputStream append = null;
						if (localResponseFileFile.exists()) {
							localResponseFileFile.renameTo(localResponseBadFileFile);
							logger.fine("renamed local response to bad file " + localResponseBadFileFile);

						}
						append = new FileOutputStream(localResponseDoneFileFile);

						PrintStream pr = new PrintStream(append);
						pr.println("");
						e.printStackTrace(pr);
						pr.close();
						logger.fine("copied error  to marker " + localResponseDoneFileFile);

                        if (remoteFileExists(c, remoteFilesDir)) {
                            SftpATTRS stat = c.stat(remoteResponseFile);
                            if (stat != null) {
                                c.rename(remoteResponseFile, remoteResponseBadFile);
                                logger.fine("renamed remote response to bad file " + remoteResponseBadFile);
                            }
                            append = c.put(remoteResponseDoneFile);

                            pr = new PrintStream(append);
                            pr.println("");
                            e.printStackTrace(pr);
                            pr.close();
                            logger.fine("copied error to remote marker " + remoteResponseDoneFile);
                        } else {
                            logger.fine("remote directory already gone " + remoteFilesDir);
                        }
					} catch (Exception e2) {
						logger.log(Level.SEVERE, "caught exception while handling error  ", e);

					}
					throw e;
				} finally {
					if (remoteRequestStream != null) {
						try {
							remoteRequestStream.close();
                            logger.finer("closed remote "+ remoteRequestFile);
						} catch (Exception e) {
                            logger.fine("failed to close remote "+ remoteRequestFile + " "+e);
						}
					}
					if (localRequestStream != null) {
						try {
							localRequestStream.close();
                            logger.finer("closed local "+ localRequestFile);
						} catch (Exception e) {
                            logger.fine("failed to close local "+ localRequestFile + " "+e);
						}
					}
					if (localResponseStream != null) {
						try {
							localResponseStream.close();
                            logger.finer("closed local "+ localResponseFile);
						} catch (Exception e) {
                            logger.fine("failed to close local "+ localResponseFile + " "+e);
						}
					}
					if (remoteResponseStream != null) {
						try {
							remoteResponseStream.close();
                            logger.finer("closed remote "+ remoteResponseFile);
						} catch (Exception e) {
                            logger.fine("failed to close remote "+ remoteResponseFile + " "+e);
						}
					}
					c.disconnect();
				 
				}
                logger.fine("done with " + dirName);

			} catch (Exception  t) {
				logger.log(Level.SEVERE, "caught while processing dir " + dirName, t);
			} catch (Error t) {
				logger.log(Level.SEVERE, "caught and rethrowing while processing dir " + dirName, t);
				throw new RuntimeException(t);
			}

		}
 
		private boolean remoteFileExists(ChannelSftp c, String remoteRequestFile) throws SftpException {
			try {
				SftpATTRS stat = c.stat(remoteRequestFile);
				return stat.isReg() || stat.isDir();
			} catch (SftpException e) {

				if (e.getMessage().equals("No such file"))
					return false;
				throw e;
			}
		}

	};

	public class MyUserInfo implements UserInfo {
		public String getPassword() {
			return password;
		}

		public boolean promptYesNo(String str) {
			return true;
		}

		public String getPassphrase() {
			return null;
		}

		public boolean promptPassphrase(String message) {
			return false;
		}

		public boolean promptPassword(String message) {

			return true;
		}

		public void showMessage(String message) {
			logger.info(message);
		}
	}

	public void copyStream(InputStream is, OutputStream os, boolean withClose) throws Exception {
		byte[] buffer = new byte[256 * 1024];

		int len = 0;
		try {
			while (true) {
				boolean doneSeen = true;
				len = is.read(buffer);
				if (len >= 0) {
					os.write(buffer, 0, len);
				} else {
					if (doneSeen)
						break;
					Thread.sleep(200);

				}
			}
		} finally {
			if (withClose) {
				if (os != null)
					os.close();
				os = null;
				if (is != null)
					is.close();
			}
		}

	}

	public int copyStreamNonBlocking(InputStream is, OutputStream os) throws Exception {
		byte[] buffer = new byte[64 * 1024];

		int overall = 0;
		int len = 0;

		while (is instanceof FileInputStream ? is.available() > 0 : true) {

			len = is.read(buffer);
			if (len >= 0) {
				logger.fine("read " + len + " bytes");
				os.write(buffer, 0, len);
				overall += len;

			} else {
				logger.finest("nothing available");
				break;
			}
		}

		if (overall > 0)
			os.flush();

		return overall;

	}

}
