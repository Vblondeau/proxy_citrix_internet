#****************************************************
#                                                   *
#               HTTP PROXY                          *
#               Version: 1.0                        *
#               Author: Luu Gia Thuy                *
#                                                   *
#****************************************************

import os,sys,thread,socket,logging,re
from datetime import datetime
import select
import os, errno, time
import pprint

baseDir =  '//silnas100/citrix$/homedir_cesario/'+os.environ['USERNAME']+'.PRODGRP/proxyrequests'
#baseDir = '/cygdrive/c/Users/gibart_j/proxyrequests'
#NTLM proxy
proxyserver = "127.0.0.1"
proxyport = 5865

ignoreDirs = []
logging.basicConfig(level=logging.INFO)
READ_BLOCK_SIZE = 256 * 1024

#********* CONSTANT VARIABLES *********
DEBUG = True            # set to True to see the debug msgs

#**************************************
#********* MAIN PROGRAM ***************
#**************************************
def main():

    # check the length of command running
    if (len(sys.argv)<2):
        print ("no basedir passed, using : "+baseDir) 

    # host and port info.
    
    print ( "FromDiskProxy Server Running on %s" %(baseDir ) )
    ensureCreated(baseDir)

    # scan dir
    # r=root, d=directories, f = files
    for r, d, f in os.walk(baseDir):
      for dir in d:
        if not '.processed' in dir:
           ignoreDirs.append(dir)
    
    # get the connection from client
    while 1:
      for r, d, f in os.walk(baseDir):
        for dir in d:
          if not '.processed' in dir:
             if not dir in ignoreDirs:
                # create a thread to handle request
                ignoreDirs.append(dir)
                thread.start_new_thread(proxy_thread, (r, dir))


        
        
    s.close()
#************** END MAIN PROGRAM ***************

def ensureCreated(directory):
    try:
      os.makedirs(directory)
    except OSError as e:
      if e.errno != errno.EEXIST:
        raise


#*******************************************
#********* PROXY_THREAD FUNC ***************
# A thread to handle request from browser
#*******************************************
def proxy_thread(r, d):


    filesDir = r + "/" + d
    logging.info("starting new conversation "+d)
    responseFile = filesDir+ "/response.txt"
    requestFile = filesDir+ "/request.txt"
    closedFile = filesDir+ "/closed.txt"

    while not  os.path.isfile(requestFile):
      if   os.path.isfile(closedFile):
        logging.debug("request in error already")
        return
      time.sleep(0.2)
    # get the request from browser
    input = open(requestFile, 'rb')
    #input.setblocking(0)
    #fd = input.fileno()
    #fcntl.fcntl(fd, fcntl.F_SETFL, flag | os.O_NONBLOCK)
    output = None
    s = None
    
    
#    requestBytes = input.read(64*1024)
#    request = requestBytes.decode('utf-8')


    
    try:
        # create a socket to connect to the web server
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  
        logging.debug("Connecting to %s %s " % (proxyserver, proxyport))
        s.connect((proxyserver, proxyport))
        logging.debug("Connected to %s %s " % (proxyserver, proxyport))
        s.setblocking(0)
        
        read_fds = [s, input]
        write_dfs = [ s ]
        sendBuffer = bytearray(0)
        while True:
              isClosed = os.path.isfile(closedFile)
              readable, writable, exceptional  = select.select(read_fds, write_dfs , [] , 1.0)
              
              #logging.debug("readable "+pprint.pformat(readable))
              #logging.debug("writable "+pprint.pformat(writable))
              if input in readable:
                requestBytes = input.read(READ_BLOCK_SIZE)
                if len (requestBytes ):
                  logging.debug ( "got %d bytes from file %s  "% (len(requestBytes),requestFile))
                  sendBuffer.extend(requestBytes)
              if s in readable:
                logging.debug("socket readable");
                data = s.recv(READ_BLOCK_SIZE)
                if (len(data) > 0):
                   logging.debug ( "got  %d bytes from socket  "% (len(data)))
                   # send to browser
                   if output is None :
                      output = open( responseFile, "wb")
                   output.write(data)
                   output.flush()
                   logging.debug ( "wrote %d bytes to disk file %s  "% (len(data), responseFile))
                else:
                  logging.debug ( "detected closed socket on read")
                  isClosed = True
              if  s in writable and len(sendBuffer) >0:
                  logging.debug("socket writable");
                  sent = s.send(sendBuffer)
                  if sent == 0:
                    isClosed = True
                    logging.debug ( "detected closed socket on write")
                  else:
                    sendBuffer = sendBuffer[sent:]
                    logging.debug ( "wrote %d bytes to socket "% (sent))
                  
              if isClosed and len (sendBuffer)  == 0:
                break
    except socket.error as message :
        logging.error("caught %s  "% (str(message) ), exc_info=True)
    finally:
      if s is not None:
        s.close()
      if output is not None:
        output.close()
      input.close()
      if not os.path.isfile(closedFile):
        closedDesc = open(closedFile, 'wb')
        closedDesc.close()
        logging.debug ( "created  "+ closedFile )
      logging.info ( "closed conversation "+ filesDir )

#********** END PROXY_THREAD ***********
    

if __name__ == '__main__':
    main()




